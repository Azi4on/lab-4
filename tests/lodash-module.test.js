import { flip, join, pullAt, remove, union } from '../src/module';
import _ from 'lodash';
import { expect, test } from '@jest/globals';

test('Lodash join result equals custom join', () => {
  (function () {
    const array = ['a', 'b', 'c'];
    const separator = '_';
    expect(join(array, separator)).toEqual(_.join(array, separator));
  })();
  (function () {
    const array = [];
    const separator = '_';
    expect(join(array, separator)).toEqual(_.join(array, separator));
  })();
  (function () {
    const array = 123;
    const separator = '_';
    expect(join(array, separator)).toEqual(_.join(array, separator));
  })();
});

test('Lodash pullAt result equals custom pullAt', () => {
  (function () {
    const array = ['a', 'b', 'c', 'd'];
    const arrayCopy = [...array];
    const removeIndices = [1, 3];
    expect(pullAt(array, removeIndices)).toEqual(
      _.pullAt(arrayCopy, removeIndices)
    );
    expect(array).toEqual(arrayCopy);
  })();
  (function () {
    const array = [];
    const arrayCopy = [...array];
    const removeIndices = [1, 3];
    expect(pullAt(array, removeIndices)).toEqual(
      _.pullAt(arrayCopy, removeIndices)
    );
    expect(array).toEqual(arrayCopy);
  })();
  (function () {
    const array = 123;
    const arrayCopy = 987;
    const removeIndices = [1, 3];
    expect(pullAt(array, removeIndices)).toEqual(
      _.pullAt(arrayCopy, removeIndices)
    );
  })();
});

test('Lodash remove result equals custom remove', () => {
  function isEven(number) {
    return number % 2 === 0;
  }

  function isLessThanThree(number) {
    return number < 3;
  }

  function passThrough(prop) {
    return prop;
  }

  (function () {
    const array = [1, 2, 4, 5];
    const arrayCopy = [...array];
    const checkFunction = isEven;

    expect(remove(array, checkFunction)).toEqual(
      _.remove(arrayCopy, checkFunction)
    );
    expect(array).toEqual(arrayCopy);
  })();
  (function () {
    const array = [1, 2, 4, 5];
    const arrayCopy = [...array];
    const checkFunction = isLessThanThree;

    expect(remove(array, checkFunction)).toEqual(
      _.remove(arrayCopy, checkFunction)
    );
    expect(array).toEqual(arrayCopy);
  })();
  (function () {
    const array = [];
    const arrayCopy = [...array];
    const checkFunction = passThrough;

    expect(remove(array, checkFunction)).toEqual(
      _.remove(arrayCopy, checkFunction)
    );
    expect(array).toEqual(arrayCopy);
  })();
});

test('Lodash union result equals custom union', () => {
  (function () {
    const first = [1, 2];
    const second = [2, 3];

    expect(union(first, second)).toEqual(_.union(first, second));
  })();
  (function () {
    const first = [1, 2];
    const second = [];

    expect(union(first, second)).toEqual(_.union(first, second));
  })();
  (function () {
    const first = [1, 2];
    const second = [-10, -20, 'hello'];

    expect(union(first, second)).toEqual(_.union(first, second));
  })();
});

test('Lodash flip result equals custom flip', () => {
  function flipperFloyd(...args) {
    return args;
  }

  (function () {
    const args = [1, 2, 3, 4, 5];

    const customFlipped = flip(flipperFloyd);
    const lodashFlipped = _.flip(flipperFloyd);

    expect(customFlipped(...args)).toEqual(lodashFlipped(...args));
  })();
});
